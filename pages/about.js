import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Layout from '../components/Layout'

import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from "next-i18next";

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['navbar', 'about', 'footer'])),
      // Will be passed to the page component as props
    },
  };
}

export default function AboutMe() {
  const { t } = useTranslation('about');
  return (
    <Layout>
        <h1 style={{paddingTop: 50, paddingBottom: 25}}>{t('menu_about_me')}</h1>
        <div className={"row"}>
            <div className={"col-xs-12 col-lg-4"}>
                <img src="../Portrait.jpg" className={"img-fluid"} alt="Picture"></img>
                <p className={"text-center"} style={{fontSize: 12}}>{t('about_photo_by')} <a href="https://guillaumebouju.fr/index.php">Guillaume Bouju</a></p>
            </div>
            <div className={"col-xs-12 col-lg-8"}>
                <p className={"text-left"} style={{fontSize: "large"}}>{t('about_intro')}</p>
                <p className={"text-left"} style={{fontSize: "medium"}}>{t('about_presentation')}</p>
                <p className={"text-left"} style={{fontSize: "medium"}}>{t('about_hobbies')}</p>
            </div>
        </div>
    </Layout>
  )
}
