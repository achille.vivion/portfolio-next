import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Layout from '../components/Layout'

import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from "next-i18next";

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['navbar', 'common', 'footer'])),
      // Will be passed to the page component as props
    },
  };
}

export default function Home() {
  const { t } = useTranslation('common');
  return (
    <Layout>
      <h1 className={"text-center"} style={{paddingTop: 50}}>{t('welcome')}</h1>
      <div className="d-flex justify-content-center align-items-center">
        <img src="https://media.giphy.com/media/bcKmIWkUMCjVm/giphy.gif" className={"img-fluid"} alt="Welcome gif"></img>
      </div>
      <p className={"text-center"} style={{fontSize: 12}}>src : <a href="https://giphy.com/">Giphy</a></p>

    </Layout>
  )
}
