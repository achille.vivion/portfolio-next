import Head from 'next/head'
import styles from '../../styles/Home.module.css'
import Layout from '../../components/Layout'

import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { i18n } from "next-i18next";

import utilStyles from '../../styles/utils.module.css'
import { getSortedPostsData } from '../../lib/blog'

import Link from 'next/link'
import Date from '../../components/Date'

export async function getStaticProps({ locale }) {
    const allPostsData = getSortedPostsData()
    return {
        props: {
            allPostsData,
            ...(await serverSideTranslations(locale, ['navbar', 'footer'])),
            // Will be passed to the page component as props
        },
    };
}

export default function Blog({ allPostsData }) {
  return (
    <Layout>
      <h1 style={{paddingTop: 50, paddingBottom: 25}}>Blog</h1>
      <div className={"row"}>
        {allPostsData.map(({ id, date, title }) => (
          <div className={"col-sm-6 col-md-4 col-lg-3"}>
            <Link href={'/blog/'+id} locale={i18n.language}>
              <a>
                <div className={"card"}>
                  <img className={"card-img-top"} src={"../../blog/"+id+".jpg"} alt={"Card image cap"}></img>
                  <div className={"card-body"}>
                    <h5 className={"card-title"}>{title}</h5>
                    <small className={utilStyles.lightText}>
                      <Date dateString={date} />
                    </small>
                  </div>
                </div>
              </a>
            </Link>
            <br />
          </div>
        ))}
      </div>
    </Layout>
  )
}