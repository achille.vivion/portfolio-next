import Head from 'next/head'
import Layout from '../../components/Layout'
import Date from '../../components/Date'
import { getAllPostIds, getPostData } from '../../lib/blog'

import { serverSideTranslations } from 'next-i18next/serverSideTranslations';

import utilStyles from '../../styles/utils.module.css'

export default function Post({ postData }) {
  //console.log(getAllPostIds());
  return (
    <Layout>
        <Head>
            <title>{postData.title}</title>
        </Head>
        <article>
            <h1 className={utilStyles.headingXl}>{postData.title}</h1>
            <img className={"card-img-top"} src={"../../blog/"+postData.id+".jpg"} alt={"Illustration"}></img>
            <div className={utilStyles.lightText}>
                <Date dateString={postData.date} />
            </div>
            <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} />
        </article>
    </Layout>
  );
}

export async function getStaticPaths() {
    const paths = getAllPostIds()
    return {
      paths,
      fallback: false
    }
}

export async function getStaticProps({ params, locale }) {
    const postData = await getPostData(params.id)
    return {
      props: {
        postData,
        ...(await serverSideTranslations(locale, ['navbar', 'footer'])),
            // Will be passed to the page component as props
      }
    }
}