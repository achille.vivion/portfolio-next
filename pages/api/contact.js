import { mailer } from '../../config';
export default (req, res) => {
  var nodemailer = require('nodemailer');
  var transporter = nodemailer.createTransport({
    service: mailer.service,
    auth: {
      user: mailer.user,
      pass: mailer.pass
    }
  });
  const mailOptions = {
    from: mailer.user, // sender address
    to: mailer.to, // list of receivers
    subject: req.body.subject, // Subject line
    html: '<h3>'+req.body.email+'</h3><br></br><p>'+req.body.message+'</p>'// plain text body
  };
  transporter.sendMail(mailOptions, function (err, info) {
    if(err)
      console.log(err)
    else
      console.log(info);
  });
  res.redirect(req.headers.referer);
}