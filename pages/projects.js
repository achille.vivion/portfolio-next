import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Layout from '../components/Layout'

import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from "next-i18next";

import Project from "../components/Project";

import { getProjectsData } from '../lib/projects'

export async function getStaticProps({ locale }) {
  const projects = getProjectsData();
  return {
    props: {
      projects,
      ...(await serverSideTranslations(locale, ['navbar', 'projects', 'footer'])),
      // Will be passed to the page component as props
    },
  };
}

export default function Projects({ projects }) {
  const { t } = useTranslation('projects');
  const listProjects = projects.map((project) => {
    return(<Project key={project.id} data={project} />)
  });
  return (
    <Layout>
        <h1 style={{paddingTop: 50, paddingBottom: 15}}> {t('menu_personnal_projects')} </h1>
        <div className={"row"}>
            {listProjects}
        </div>
    </Layout>
  )
}
