import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Layout from '../components/Layout'

import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from "next-i18next";

export async function getStaticProps({ locale }) {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['navbar', 'contact', 'footer'])),
      // Will be passed to the page component as props
    },
  };
}

export default function Contact() {
  const { t } = useTranslation('contact');
  return (
    <Layout>
        <h1 style={{paddingTop: 50}}>{t('menu_contact')}</h1>
        <form method="post" action="/api/contact/">
            <div className="form-group">
                <input type="email" className="form-control" name="email" placeholder={t('contact_email')} required></input>
            </div>
            <div className="form-group">
                <input type="text" className="form-control" name="subject" placeholder={t('contact_subject')} required></input>
            </div>
            <div className="form-group">
                <textarea type="text" className="form-control" name="message" rows="9" placeholder={t('contact_message')} required></textarea>
            </div>
            <button type="submit" className="btn btn-dark">{t('contact_submit')}</button>
        </form>
    </Layout>
  )
}
