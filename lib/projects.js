import fs from 'fs'
import path from 'path'

const projectsDirectory = path.join(process.cwd(), 'projects')

export function getProjectsData() {
    // Get file names under /posts
    const fileNames = fs.readdirSync(projectsDirectory)
    return fileNames.map(fileName => {
      // Remove ".json" from file name to get id
      const id = fileName.replace(/\.json$/, '')
    
      // Read data
      const fullPath = path.join(projectsDirectory, fileName)
      const data = JSON.parse(fs.readFileSync(fullPath, 'utf8'))
  
      // Combine the data with the id
      return {
        id,
        ...data
      }
    })
  }