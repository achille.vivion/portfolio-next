// Dependencies
const fs = require('fs');
const https = require('https');
const { createServer } = require('http');

const next = require('next')
const port = 443
const port_http = 80
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev, dir: __dirname })
const handle = app.getRequestHandler()

// Certificate
const privateKey = fs.readFileSync('/etc/letsencrypt/live/avivion.fr/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/avivion.fr/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/letsencrypt/live/avivion.fr/chain.pem', 'utf8');

const credentials = {
	key: privateKey,
	cert: certificate,
	ca: ca
};

app.prepare().then(() => {
  https.createServer(credentials, (req, res) => {
      handle(req, res);
  }).listen(port, err => {
      if (err) throw err
      console.log(`> Ready on localhost:${port}`)
  })
})

app.prepare().then(() => {
  createServer( (req, res) => {
      handle(req, res);
  }).listen(port_http, err => {
      if (err) throw err
      console.log(`> Ready on localhost:${port_http}`)
  })
})