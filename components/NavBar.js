import Link from 'next/link'
import { useRouter } from 'next/router'

import { useTranslation, i18n } from "next-i18next";

import { faHome } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import 'flag-icon-css/css/flag-icon.min.css'

export default function NavBar () {
    const router = useRouter();
    const { t } = useTranslation('navbar');
    const langues = i18n.languages.map((value) => {
        var flag = "gb";
        if(value != "en") {
            flag = value;
        }

        return (
            <button key={value} className={"dropdown-item"} onClick={() => router.push(router.asPath, router.asPath, { locale : value})}>
                <span className={"flag-icon flag-icon-"+flag}></span>
            </button>
        );
    });
    var flag = "gb";
    if(i18n.language != "en") {
        flag = i18n.language;
    }
    return(
        <nav className={"navbar navbar-expand-md navbar-dark bg-dark"} style={{minHeight: 10 + 'vh'}}>
            <Link href={"/"} locale={i18n.language}>
                <a className={"navbar-brand"}>
                    <FontAwesomeIcon icon={faHome} size={'2x'}/>
                </a>
            </Link>
            <button className={"navbar-toggler"} type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-expanded="false" aria-controls="navbarSupportedContent" aria-label="Toggle navigation">
                <span className={"navbar-toggler-icon"}></span>
            </button>
            <div className={"collapse navbar-collapse"} id="navbarSupportedContent">
                <ul className={"navbar-nav mr-auto"}>
                    <li className={"nav-item"}>
                        <Link href={"/projects/"} locale={i18n.language}>
                             <a className={"nav-link"}>{t('menu_personnal_projects')}</a>
                        </Link>
                    </li>
                    <li className={"nav-item"}>
                        <Link href={"/about/"} locale={i18n.language}>  
                            <a className={"nav-link"}>{t('menu_about_me')}</a>
                        </Link>
                    </li>
                    <li className={"nav-item"}>
                        <Link href={"/contact/"} locale={i18n.language}>  
                            <a className={"nav-link"}>{t('menu_contact')}</a>
                        </Link>
                    </li>
                    <li className={"nav-item"}>
                        <Link href={"/blog/"} locale={i18n.language}>
                            <a className={"nav-link"}>Blog</a>
                        </Link>
                    </li>
                </ul>
                <div className={"nav-item dropdown"}>
                    <a className={"nav-link dropdown-toggle"} href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span className={"navbar-text flag-icon flag-icon-"+flag}></span>
                    </a>
                    <div className={"dropdown-menu"} aria-labelledby="navbarDropdown">
                        {langues}
                    </div>
                </div>
            </div>
        </nav>
    )
}