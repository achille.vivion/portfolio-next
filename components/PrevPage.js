import React from 'react';
import { Link, withRouter } from 'react-router-dom';

import { Col } from 'reactstrap';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronCircleLeft } from '@fortawesome/free-solid-svg-icons'

function PrevPage (props){
    return(
        <Col xs={2} className="text-center align-self-center">
            <Link className={"text-dark"} to={props.path}>
                <FontAwesomeIcon icon={faChevronCircleLeft} size="2x" />
            </Link>    
        </Col>
    );
}

export default withRouter(PrevPage);