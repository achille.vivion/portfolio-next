//import { useTranslation } from "react-i18next";
//import '../i18n';

import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInstagram, faLinkedin } from '@fortawesome/free-brands-svg-icons'

import { useTranslation } from "next-i18next";

export default function Footer () {
    const { t } = useTranslation('footer');
    return(
        <footer className="bg-dark text-white" style={{minHeight: 10 + 'vh'}}>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-12 col-md-2 text-center align-self-center">
                        <ul className="list-inline">
                            <li className="list-inline-item"><a className="social-icon text-center text-white" target="_blank" href="#">
                            <FontAwesomeIcon icon={faInstagram} />
                            </a></li>
                            <li className="list-inline-item"><a className="social-icon text-center text-white" target="_blank" href="https://www.linkedin.com/in/achille-vivion-083560172">
                            <FontAwesomeIcon icon={faLinkedin} />
                            </a></li>
                        </ul>
                    </div>
                    <div className="col-sm-12 col-md-10 text-center">
                        <p><b>{t('footer_technos')}</b></p>
                        <ul className="list-inline">
                            <li className="list-inline-item">
                                <a className="social-icon text-center text-white" target="_blank" href="https://reactjs.org/">NextJS</a>
                            </li>
                            <span> - </span>
                            <li className="list-inline-item">
                                <a className="social-icon text-center text-white" target="_blank" href="https://getbootstrap.com/">Bootstrap</a>
                            </li>
                            <span> - </span>
                            <li className="list-inline-item">
                                <a className="social-icon text-center text-white" target="_blank" href="https://react.i18next.com/">i18next</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    )
}