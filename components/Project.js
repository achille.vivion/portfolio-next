import { i18n } from "next-i18next";

export default function Project(props){
    return (
        <div className="col-xs-6 col-md-4 col-lg-3">
            <button style={{height: "75px", width: "100%", marginBottom: "10px"}} type="button" className="btn btn-dark" data-toggle="modal" data-target={"#modal-project-"+props.data.id}>
                {props.data.titre}
            </button>

            <div className="modal fade" id={"modal-project-"+props.data.id} role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{props.data.titre}</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <p>{props.data.desc[i18n.language]}</p>
                    </div>
                    <div className="modal-footer">
                        <a href={props.data.git}>{props.data.git}</a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    );
}